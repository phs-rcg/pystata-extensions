PyStata Extensions
==================

Extensions to PyStata to facilitate use of Stata within Jupyter notebooks.
Currently provides the following features:

1. Easier initialization, e.g.::

       import pystata_extensions as pse

2. Patch to ``dataframe_to_stata()`` to translate categorical type into
   integer variable with value labels, and to optimize all final storage types.

3. Function ``collect_preview()`` for displaying tables created from
   collections.


Initialization
--------------

Initialization happens automatically on import, if one of the following is
true:

1. The pystata package, installed automatically by the Stata installer, is
   on your PYTHONPATH. This can be done from within your ``.bash_profile``,
   e.g.::

       PYTHONPATH=/Applications/Stata/utilities:${PYTHONPATH}
       export PYTHONPATH

2. A configuration file called ``config.yaml`` is found in any of the standard
   places, e.g., ``~/.config/pystata`` or the following::

       macOS: ~/Library/Application Support/pystata
       Unix: /etc/pystata
       Windows: %APPDATA%\pystata or %HOME%\AppData\Roaming\pystata

   The configuration file may include the following items::

       path: [Stata's installation path]
       edition: [be, se or mp]

If the Stata edition is not found, initialization will try the following in
order: ``mp``, ``se`` and ``be``.


Displaying Tables
-----------------

This may be done with the following::

    pse.collect_preview([cname])
