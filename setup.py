from setuptools import setup, find_namespace_packages

with open('README.rst', 'r') as fh:
    long_description = fh.read()

setup(
    name='pystata_extensions',
    version='0.1.0',
    author='Phil Schumm',
    author_email='pschumm@uchicago.edu',
    description='Extensions to PyStata package',
    long_description=long_description,
    long_description_content_type='text/x-rst',
    url='https://gitlab.com/phs-rcg/pystata-extensions',
    package_dir={'': 'src'},
    packages=find_namespace_packages(where='src'),
    install_requires=[
        'confuse',
        'pandas',
        'lxml',
        'beautifulsoup4',
        'cssutils'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
    ]
)
