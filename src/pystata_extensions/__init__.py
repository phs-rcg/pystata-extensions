import confuse
import contextlib
import os
import numpy as np
import pandas as pd
from io import StringIO
from bs4 import BeautifulSoup
import cssutils
import re
from IPython.display import display
import pprint

config = confuse.Configuration('pystata')

try:
    import pystata

except ModuleNotFoundError:

    import sys

    try:
        path = config['path'].as_path()
    except confuse.NotFoundError:
        raise Exception('Path to Stata installation not found; try adding to config.yaml file') from None

    if not os.path.isdir(path):
        raise OSError(f'{path} is not a valid directory path on the system') from None

    if not os.path.isdir(os.path.join(path, 'utilities')):
        raise OSError(f"{path} is not Stata's installation path") from None

    sys.path.append(os.path.join(path, 'utilities'))
    import pystata

with contextlib.redirect_stdout(None):

    try:
        editions = [config['edition'].as_choice(['mp','se','be'])]
    except confuse.NotFoundError:
        editions = ['mp','se','be']

    for edition in editions:
        try:
            pystata.config.init(edition, splash=False)
        except FileNotFoundError:
            version = None
        else:
            version = pystata.config._get_stata_version_str()
            break

if version:
    print(version)
else:
    raise Exception('Unable to initialize Stata')

# Monkey patch dataframe_to_stata() to handle categoricals
# Also adds call to compress to optimize final storage types
def _add_category_var(dta, col):
    sfi = pystata.core.pandas2stata.sfi

    sfi.ValueLabel.createLabel(col)
    for num, val in enumerate(dta.cat.categories):
        sfi.ValueLabel.setLabelValue(col, num+1, str(val))

    sfi.Data.addVarLong(col)
    sfi.ValueLabel.setVarValueLabel(col, col)

def dataframe_to_stata(df, stfr):
    if not isinstance(df, pd.DataFrame):
        raise TypeError("A Pandas dataframe is required.")

    sfi = pystata.core.pandas2stata.sfi
    _make_varname = pystata.core.pandas2stata._make_varname
    _add_var = pystata.core.pandas2stata._add_var

    nobs = len(df)
    if nobs == 0:
        return None

    cf = sfi.Frame.connect(sfi.Macro.getGlobal('c(frame)'))
    if stfr:
        fr = sfi.Frame.create(stfr)
        # Change frame only way to create value labels
        fr.changeToCWF()

    colnames = list(df.columns)
    stnames = []

    sfi.Data.setObsTotal(nobs)
    var = 0
    for col in colnames:
        vtype = df.dtypes[col].name
        dta = df[col]
        col = _make_varname(col, var+1, stnames, colnames)
        if vtype=="int_" or vtype=="int8" or vtype=="int16" or vtype=="int32" or \
            vtype=="uint8" or vtype=="uint16":
            _add_var(col, "int")
            sfi.Data.store(var, None, dta)
        elif vtype=="int64" or vtype=='uint32':
            _add_var(col, "long")
            sfi.Data.store(var, None, dta)
        elif vtype=="float_" or vtype=="float16" or vtype=="float32":
            _add_var(col, "float")
            sfi.Data.store(var, None, dta)
        elif vtype=="float64":
            _add_var(col, "double")
            sfi.Data.store(var, None, dta)
        elif vtype=="bool":
            _add_var(col, "byte")
            sfi.Data.store(var, None, dta)
        elif vtype=="category":
            _add_category_var(dta, col)
            sfi.Data.store(var, None, dta.cat.codes.replace(-1,np.nan))
        else:
            _add_var(col, "str")
            sfi.Data.store(var, None, dta.astype(str))

        var += 1

    sfi.SFIToolkit.stata('qui compress')
    cf.changeToCWF()

pystata.core.pandas2stata.dataframe_to_stata = dataframe_to_stata
del dataframe_to_stata

# Monkey patch _print_streaming_output() to remove trailing prompt (".")
def _print_streaming_output(output, newline):
    config = pystata.config
    if output[-4:]=='\n\n. ':
        output = output[:-3]
    if config.pyversion[0] >= 3:
        if newline and output:
            print(output, flush=True, file=config.stoutputf)
        else:
            print(output, end='', flush=True, file=config.stoutputf)
    else:
        if newline:
            print(config.get_encode_str(output), file=config.stoutputf)
        else:
            print(config.get_encode_str(output), end='', file=config.stoutputf)

        sys.stdout.flush()

pystata.core.stout._print_streaming_output = _print_streaming_output
del _print_streaming_output

def _extract_css_rules(soup):
    """Extract CSS rule set from Stata-generated HTML for one or more tables."""

    style = soup.select('style')
    if style:
        css = cssutils.parseString(style[0].encode_contents())
    else:
        css = []

    rules = {'tables':{}}
    for rule in css:
        if rule.type == rule.STYLE_RULE and rule.style.length:
            declarations = {}
            for item in rule.style:
                declarations[item.name] = item.value

            m = re.match(r'^\.(.+?)_([0-9x]+)_([0-9x]+)(_span)?$', rule.selectorText)
            if re.match(r'^table$', rule.selectorText):
                rules['table'] = declarations
            elif re.match(r'^\.[^_]+_title$', rule.selectorText):
                rules['title'] = declarations
            elif m:
                key = (int(m.group(2)), int(m.group(3)))
                if m.group(1) in rules['tables']:
                    if key in rules['tables'][m.group(1)]:
                        rules['tables'][m.group(1)][key].update(declarations)
                    else:
                        rules['tables'][m.group(1)][key] = declarations
                else:
                    rules['tables'][m.group(1)] = {key:declarations}

    return rules

def _cnt_nbsp(s):
    """Return consecutive number of &nbsp; on left and right of string"""

    m = re.match(r'^([\u00a0]*).+?([\u00a0]*)$', s.strip(' \r\n'))
    left = len(m.group(1)) if m and m.group(1) else None
    right = len(m.group(2)) if m and m.group(2) else None

    return left, right

def _add_padding(css, table, stub_padding, cell_padding):
    """Add padding to CSS in place of &nbsp;"""

    r = 1
    for row in table.find_all('tr'):

        lp, rp = _cnt_nbsp(row.find_next('th').find_next('span').get_text())
        if lp:
            css[(r,1)]['padding-left'] = f'{lp*stub_padding}ch'

        c = 2
        for column in row.find_all('td'):
            lp, rp = _cnt_nbsp(column.find_next('span').get_text())
            if lp:
                css[(r,c)]['padding-left'] = f'{lp*cell_padding}ch'
            if rp:
                css[(r,c)]['padding-right'] = f'{rp*cell_padding}ch'
            c += 1

        r += 1

def _style_header_row(columns, row, css, excl):
    """Style a single header row."""

    styles = []
    for column in range(len(columns)):
        styles.append(';'.join([f'{n}:{v}' for n, v in css[(row+1,column+2)].items()
                                if n not in excl]))

    return styles

def _style_stub(rows, hdr_rows, css, excl):
    """Style table stub."""

    styles = []
    for row in range(len(rows)):
        styles.append(';'.join([f'{n}:{v}' for n, v in css[(row+hdr_rows+1,1)].items()
                                if n not in excl]))

    return styles

def _style_cell(array, r, c, hdr_rows, css, excl):
    """Style individual table cell."""

    array[r,c] = ';'.join([f'{k}:{v}' for k, v in css.get((r+hdr_rows+1,c+2),{}).items()
                           if k not in excl])

def _style_cells(df, css, excl):
    """Style table cells."""

    rows, columns = df.shape
    hdr_rows = df.columns.nlevels
    styles = np.full((rows, columns), '', dtype='object')

    for r in range(rows):
        for c in range(columns):
            _style_cell(styles, r, c, hdr_rows, css, excl)

    return styles

def _style_table(df, css, line_height=16, style_cells=True, header_excl=None,
                 stub_excl=None, cell_excl=None):
    """Style table stored as Pandas data frame."""

    excl = ['border-top-style','border-bottom-style','border-left-style',
            'border-right-style','vertical-align']
    if header_excl is None:
        header_excl = excl
    if stub_excl is None:
        stub_excl = excl
    if cell_excl is None:
        cell_excl = excl

    hdr_rows = df.columns.nlevels
    rules = css['tables'][df.attrs['name']]
    styler = df.style

    for row in range(hdr_rows):
        styler.apply_index(_style_header_row, axis=1, level=row, row=row,
                           css=rules, excl=header_excl)

    styler.apply_index(_style_stub, axis=0, hdr_rows=hdr_rows, css=rules,
                       excl=stub_excl)

    if style_cells and df.index.is_unique:
        styler.apply(_style_cells, axis=None, css=rules, excl=cell_excl)
    elif style_cells:
        print('Note: Cell styling skipped due to non-unique index; use '
              'style_cells=False to suppress.')

    return (styler
        .set_caption(df.attrs['title'])
        .set_table_styles([
            {'selector': 'tr', 'props': f'line-height: {line_height}pt;'},
            {'selector': 'td,th',
             'props': 'line-height: inherit; padding-top: 0; padding-bottom: 0;'}
        ])
    )

def collect_preview(name=None, select=None, display_tables=True, stub_padding=1.25,
                    cell_padding=1, verbose=False, **kwargs):
    """Display table(s) created from a collection.

    Parameters
    ----------
    name : str, optional
        Name of collection to display (otherwise display current collection).
    select : list of int, optional
        List of tables (using 0-based indexing) to return/display if layout
        generates multiple tables. By default, all tables are displayed as HTML
        and returned (use ';' to avoid second, non-HTML display).
    display_tables : bool, default True
        Set to False to avoid HTML display of tables when multiple tables are
        returned.
    stub_padding : int or float, default 1.25
        Numer of ch units to subtitute for each &nbsp; when indenting stub.
    cell_padding : int or float, default 1
        Number of ch units to substitute for each &nbsp; when padding cells.
    verbose : bool, default False
        Show results of intermediate parsing and formatting steps.
    **kwargs
        Additional keywords passed through to _style_table().
    """

    stk = pystata.stata.sfi.SFIToolkit
    filename = stk.getTempFile()

    if name:
        stk.stata(f'collect style html, name({name}) useth')
        stk.stata(f'qui collect export {filename}, as(html) name({name})')
    else:
        stk.stata('collect style html, useth')
        stk.stata(f'qui collect export {filename}, as(html)')

    with open(filename) as f:
        html = f.read()
        if verbose:
            print('--- Stata HTML file ---\n', html)
    os.remove(filename)

    tables = []
    soup = BeautifulSoup(html, 'html.parser')
    css = _extract_css_rules(soup)
    titles = soup.findAll('p', class_=re.compile('.+_title$'))
    for table in soup('table'):
        name = table['class'][0]
        _add_padding(css['tables'][name], table, stub_padding, cell_padding)
        df = pd.read_html(StringIO(str(table)), index_col=0)[0]
        df = pd.read_html(StringIO(str(table)), index_col=0,
                          keep_default_na=False, thousands=None,
                          converters={c: str for c in df.columns})[0]

        # In some cases, the cell [1,1] is read as name of row index
        if df.index.name:
            df = pd.concat(
                [pd.DataFrame([[''] * len(df.columns)], columns=df.columns,
                              index=[df.index.name]), df]
            )

        if verbose:
            print('--- read_html() ---\n', df, '\n')

        df.attrs = {'name': name,
                    'title': titles.pop(0).getText().strip() if titles else ''}
        tables.append(_style_table(df, css, **kwargs))

        if verbose:
            print('--- to_html() ---\n', tables[-1].to_html(), '\n')

    if select:
        tables = [tables[i] for i in select]

    if len(tables)>1:
        if display_tables:
            for table in tables[:-1]:
                # N.B. You have to skip selector='table' to assign properties to
                # <table></table>; with selector='table' it assigns properties
                # for table inside table <table><table></table></table>.
                display(table
                        .set_table_styles([{'selector': '',
                                            'props': 'margin-bottom: 10pt'}]))
            display(tables[-1])
        return tables
    else:
        return tables[0]
